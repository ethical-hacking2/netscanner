#!/usr/bin/env python3

import scapy.all as scapy
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='ARP pinging for network scanning')
    parser.add_argument('--ip', '-i', nargs=1, required=True, type=str,
                        metavar='IP', dest='ip',
                        help='IP address or IP range of addresses to be scanned ')

    args = parser.parse_args()

    return args.ip[0]

def arp_scan(ip):
    arp_request = scapy.ARP(pdst=ip)
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast/arp_request
    answered_arp_requests = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]

    arp_scan_out = []
    for answered_arp in answered_arp_requests:
        mac_ip = {"ip": answered_arp[1].psrc, "mac": answered_arp[1].hwsrc}
        arp_scan_out.append(mac_ip)

    return arp_scan_out

def disp_arp_scan_out(arp_scan_out):
    print("IP\t\t\tMAC Address\n-----------------------------------------")
    for mac_ip in arp_scan_out:
        print(mac_ip["ip"] + "\t\t" + mac_ip["mac"])

# Parse user args
ip = parse_args()

# Perfom ARP scan
arp_scan_out = arp_scan(ip)

# Display ARP scan result
disp_arp_scan_out(arp_scan_out)

